//
//  main.m
//  QrtzDartboard
//
//  Created by John Cogan on 2014/01/01.
//  Copyright (c) 2014 GeckoDigitalSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GDSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GDSAppDelegate class]));
    }
}
