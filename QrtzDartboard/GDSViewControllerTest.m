//
//  GDSViewControllerTest.m
//  QrtzDartboard
//
//  Created by John Cogan on 2014/01/01.
//  Copyright (c) 2014 GeckoDigitalSolutions. All rights reserved.
//

#import "GDSViewControllerTest.h"

@interface GDSViewControllerTest ()

@end

@implementation GDSViewControllerTest

@synthesize viewDartBoard;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)awakeFromNib
{
    //[[self view] addSubview:dbControl];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
