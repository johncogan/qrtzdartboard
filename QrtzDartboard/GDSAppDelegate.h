//
//  GDSAppDelegate.h
//  QrtzDartboard
//
//  Created by John Cogan on 2014/01/01.
//  Copyright (c) 2014 GeckoDigitalSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GDSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
