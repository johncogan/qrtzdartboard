//
//  GDSDartboardView.m
//  QrtzDartboard
//
//  Created by John Cogan on 2014/01/01.
//  Copyright (c) 2014 GeckoDigitalSolutions. All rights reserved.
//

#import "GDSDartboardView.h"

@implementation GDSDartboardView

CGPoint centerPoint;

#define DEGREESOFARC 18.0f
// #define RADPERDEGREE 0.0174532925f;

CGContextRef context;
CGPoint centerOfView;
int numberOfSections;
CGFloat degreesPerSection;
CGPoint centerOfView;
CGFloat radiusOfBoard;
CGFloat radiusOuterOfNoScoreArea;
CGFloat doublesRadiusOuter;
CGFloat doublesThickness;
CGFloat singlesOuterRadius;
CGFloat singlesOuterThickness;
CGFloat triplesRadiusOuter;
CGFloat triplesThickness;
CGFloat singlesInnerRadius;
CGFloat singlesInnerThickness;
CGFloat radiusOfBull;
CGFloat bullThickness;
CGFloat radiusOfBullsEye;
bool isPhone;

const CGFloat DEGREES_PER_ARC = 18.0f;
const CGFloat RADPERDEGREE = 0.0174532925f;

#pragma mark - Init methods

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"GDSDartboardView - initWithFrame");
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    NSLog(@"GDSDartboardView - initWithCoder");
    //context = UIGraphicsGetCurrentContext();
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        numberOfSections = 20.0f;
        degreesPerSection = 360.0f / numberOfSections;
        centerOfView = [self getCenterPoint];
        
        // Calculate radius as been a little less that the views width to maximize size
        radiusOfBoard = (self.bounds.size.width / 2) - 5;
        
        radiusOuterOfNoScoreArea = 15.0f;
        CGFloat radiusOfNoScoreArea = ((radiusOuterOfNoScoreArea / 100) * radiusOfBoard);
        
        doublesRadiusOuter = radiusOfBoard - radiusOfNoScoreArea;
        doublesThickness = 6.0f;
        
        singlesOuterRadius = doublesRadiusOuter - ((radiusOfBoard / 100) * doublesThickness);
        singlesOuterThickness = 33.0f;
        
        triplesRadiusOuter = singlesOuterRadius - ((radiusOfBoard / 100) * singlesOuterThickness);
        triplesThickness = 6.0f;
        
        singlesInnerRadius = triplesRadiusOuter - ((radiusOfBoard / 100) * triplesThickness);
        singlesInnerThickness = 32.0f;
        
        radiusOfBull = singlesInnerRadius - ((radiusOfBoard / 100) * singlesInnerThickness);
        bullThickness = 4.0f;
        
        radiusOfBullsEye = radiusOfBull - ((radiusOfBoard / 100) * bullThickness);
        
        CGFloat totalThicknessPercent = radiusOuterOfNoScoreArea + doublesThickness + singlesOuterThickness + triplesThickness + singlesInnerThickness + bullThickness + bullThickness;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            isPhone = YES;
        }else{
            isPhone = NO;
        }
        
        NSLog(@"totalThicknessPercent = %f", totalThicknessPercent);
    }
    return self;
}

#pragma mark - Overidden methods

- (void)drawRect:(CGRect)rect
{
    NSLog(@"GDSDartboardView - drawRect");
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *redClr = [[UIColor alloc] initWithRed:[self rgbValueAsFractionOfOne:216.0f] green:[self rgbValueAsFractionOfOne:19.0f] blue:[self rgbValueAsFractionOfOne:26.0f] alpha:1.0f];
    UIColor *greenClr = [[UIColor alloc] initWithRed:[self rgbValueAsFractionOfOne:46.0f] green:[self rgbValueAsFractionOfOne:128.0f] blue:[self rgbValueAsFractionOfOne:82.0f] alpha:1.0f];
    UIColor *whiteClr = [[UIColor alloc] initWithRed:[self rgbValueAsFractionOfOne:240.0f] green:[self rgbValueAsFractionOfOne:232.0f] blue:[self rgbValueAsFractionOfOne:208.0f] alpha:1.0f];
    UIColor *blkClr = [[UIColor alloc] initWithWhite:0.0f alpha:1.0f];
    
    [self drawFilledCircle:centerOfView.x withRadius:radiusOfBoard withFillColor:[UIColor blackColor]];
    
    [self drawDoubles:261.0f withFirstColor:redClr withSecondColor:greenClr withContext:context];
    
    [self drawSinglesOuter:261.0f withFirstColor:whiteClr withSecondColor:blkClr withContext:context];
    
    [self drawTriples:261.0f withFirstColor:redClr withSecondColor:greenClr withContext:context];
    
    [self drawSinglesInner:261.0f withFirstColor:whiteClr withSecondColor:blkClr withContext:context];
    
    [self drawFilledCircle:centerOfView.x withRadius:radiusOfBull withFillColor:greenClr];
    
    [self drawFilledCircle:centerOfView.x withRadius:radiusOfBullsEye withFillColor:redClr];
    
    [self drawNumbers];
}

#pragma mark - Private methods

-(void)drawNumbers
{
    NSArray *numTextMatrix = [NSArray arrayWithObjects: @"20",@"1",@"18",@"4",@"13",@"6",@"10",@"15",@"2",@"17",@"3",@"19",@"7",@"16",@"8",@"11",@"14",@"9",@"12",@"5", nil];
    
    for(int i = 0; i <= numberOfSections-1;i++)
    {
        CGFloat currentStartDegree = (i * degreesPerSection) - 89.5f;
        
        CGFloat circularPathForText = doublesRadiusOuter;
        CGPoint pointForText;
        pointForText.x = circularPathForText;
        pointForText.y = currentStartDegree;
        
        CGFloat distanceOfTextFromCenter = doublesRadiusOuter * 1.09f;
        
        CGPoint drawPoint = [self getCartesianCoOrdinate:distanceOfTextFromCenter withAngle:currentStartDegree];
        
        NSLog(@"numberOfSections: %@", [numTextMatrix objectAtIndex:i]);
        NSLog(@"i: %i", i);
        
        CGFloat labelRotation = ((18.0f) * i) * 0.0174532925;
        
        if(i > 10)
        {
            labelRotation = (((18.0f) * i) - 0) * 0.0174532925;
        }
        
        [self drawTextInUILabel:[numTextMatrix objectAtIndex:i] withOrigin:drawPoint withRotateAngle:labelRotation];
    }
}

-(CGPoint)getCartesianCoOrdinate:(CGFloat)radius withAngle:(CGFloat)angleFromZero
{
    CGPoint pointCalcualted;
    CGFloat x, y;
    
    x = radius * cos(angleFromZero * 0.0174532925) + 5.0f;
    y = radius * sin(angleFromZero * 0.0174532925) + 5.0f;
    
    pointCalcualted.x = x + (radiusOfBoard);
    pointCalcualted.y = y + (radiusOfBoard);
    
    return pointCalcualted;
}

- (void)drawTextInUILabel:(NSString*)text withOrigin:(CGPoint)origin withRotateAngle:(CGFloat)rotateAngle;
{
    CGFloat frameWidth = 50.0f;
    if(isPhone){
        frameWidth = 30.0f;
    }
    CGFloat frameHeight = frameWidth;
    
    UIFont *numbersFont = [UIFont fontWithName:@"Noteworthy" size:40];
    if(isPhone){
        numbersFont = [UIFont fontWithName:@"Noteworthy" size:20];
    }
    
    UILabel *numLabel = [self prepareUiLabel:origin withWidth:frameWidth withHeight:frameHeight];
    numLabel.text = text;
    numLabel.textColor = [UIColor whiteColor];
    numLabel.font = numbersFont;
    
    numLabel.transform = CGAffineTransformMakeRotation(rotateAngle);
    
    [self addSubview:numLabel];
}

-(UILabel*)prepareUiLabel:(CGPoint)atOrigin withWidth:(CGFloat)labelFrameWidth withHeight:(CGFloat)labelFrameHeight
{
    NSLog(@"Preparing label");
    CGFloat frameWidth = labelFrameWidth;
    CGFloat frameHeight = labelFrameHeight;
    
    CGSize uiLabelFrame;
    uiLabelFrame.width = frameWidth;
    uiLabelFrame.height = frameHeight;
    
    CGRect uiLabelRect;
    uiLabelRect.origin = atOrigin;
    
    uiLabelRect.origin.x = uiLabelRect.origin.x - (uiLabelFrame.width/2);
    uiLabelRect.origin.y = uiLabelRect.origin.y - (uiLabelFrame.height/2);
    
    uiLabelRect.size = uiLabelFrame;
    
    UILabel *tmpUiLabel = [[UILabel alloc] initWithFrame:uiLabelRect];
    tmpUiLabel.textAlignment = NSTextAlignmentCenter;
    
    return tmpUiLabel;
}

-(CGFloat)rgbValueAsFractionOfOne:(CGFloat)rgbValue
{
    return (rgbValue / 255);
}

-(void)drawSinglesInner:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context
{
    NSLog(@"drawSinglesOuter");
    NSLog(@"singlesOuterRadius = %f", singlesInnerRadius);
    NSLog(@"singlesOuterThickness = %f", singlesInnerThickness);
    
    for(int i = 0; i <= numberOfSections;i++)
    {
        UIColor *currentColor;
        
        if(i%2 == 0){
            currentColor = colorOne;
        }else{
            currentColor = colorTwo;
        }
        
        CGFloat currentStartDegree = (i * degreesPerSection) - 99.0f;
        
        [self drawArc:[self getCenterPoint] withArcRadius:singlesInnerRadius withStartAngle:currentStartDegree withThicknessAsPercentage:singlesInnerThickness withFillColor:currentColor withContext:context];
    }
}

-(void)drawSinglesOuter:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context
{
    NSLog(@"drawSinglesOuter");
    NSLog(@"singlesOuterRadius = %f", singlesOuterRadius);
    NSLog(@"singlesOuterThickness = %f", singlesOuterThickness);
    
    for(int i = 0; i <= numberOfSections;i++)
    {
        UIColor *currentColor;
        
        if(i%2 == 0){
            currentColor = colorOne;
        }else{
            currentColor = colorTwo;
        }
        
        CGFloat currentStartDegree = (i * degreesPerSection) - 99.0f;
        
        [self drawArc:[self getCenterPoint] withArcRadius:singlesOuterRadius withStartAngle:currentStartDegree withThicknessAsPercentage:singlesOuterThickness withFillColor:currentColor withContext:context];
    }
}


-(void)drawTriples:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context
{
    NSLog(@"drawDoubles");
    NSLog(@"doublesRadiusOuter = %f", triplesRadiusOuter);
    NSLog(@"doublesThickness = %f", triplesThickness);
    
    for(int i = 0; i <= numberOfSections;i++)
    {
        UIColor *currentColor;
        
        if(i%2 == 0){
            currentColor = colorOne;
        }else{
            currentColor = colorTwo;
        }
        
        CGFloat currentStartDegree = (i * degreesPerSection) - 99.0f;
        
        [self drawArc:[self getCenterPoint] withArcRadius:triplesRadiusOuter withStartAngle:currentStartDegree withThicknessAsPercentage:triplesThickness withFillColor:currentColor withContext:context];
    }
}

-(void)drawDoubles:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context
{
    NSLog(@"drawDoubles");
    NSLog(@"doublesRadiusOuter = %f", doublesRadiusOuter);
    NSLog(@"doublesThickness = %f", doublesThickness);
    
    for(int i = 0; i <= numberOfSections;i++)
    {
        UIColor *currentColor;
        
        if(i%2 == 0){
            currentColor = colorOne;
        }else{
            currentColor = colorTwo;
        }
        
        CGFloat currentStartDegree = (i * degreesPerSection) - 99.0f;
        
        [self drawArc:[self getCenterPoint] withArcRadius:doublesRadiusOuter withStartAngle:currentStartDegree withThicknessAsPercentage:doublesThickness withFillColor:currentColor withContext:context];
    }
}

-(void)drawArc:(CGPoint)center withArcRadius:(CGFloat)arcRadius withStartAngle:(CGFloat)startAngle withThicknessAsPercentage:(CGFloat)thickness withFillColor:(UIColor*)fillColor withContext:(CGContextRef)context
{
    CGContextSaveGState(context);
    
    //Set color of current context
    CGFloat redValue = 0.0f;
    CGFloat greenValue = 0.0f;
    CGFloat blueValue = 0.0f;
    CGFloat alphaValue = 0.0f;
    
    [fillColor getRed:&redValue green:&greenValue blue:&blueValue alpha:&alphaValue];
    
    [[UIColor colorWithRed:redValue
                     green:greenValue
                      blue:blueValue
                     alpha:alphaValue] set];
    
    CGFloat thicknessAsFloat = (radiusOfBoard / 100) * thickness;
    
    // The arc thickness is drawn from the middle, so adjust the required radius for this
    CGFloat actualArcRadius = (arcRadius-(thicknessAsFloat/2));
    
    // Calculate the starting degree of the arc as radians
    CGFloat startAngleAsRadians = startAngle * RADPERDEGREE;
    
    // Calculate the ending degree of the arc as radians
    CGFloat endingAngleAsRadians = (startAngle + degreesPerSection) * RADPERDEGREE;
    
    //Draw Arc
    /*
     CGContextRef c,
     CGFloat x,
     CGFloat y,
     CGFloat radius,
     CGFloat startAngle,
     CGFloat endAngle,
     int clockwise
     */ //
    
    CGContextAddArc(context, center.x, center.y, actualArcRadius, startAngleAsRadians, endingAngleAsRadians, 0);
    CGContextSetLineWidth(context, thicknessAsFloat);
    CGContextDrawPath(context, kCGPathStroke);
    CGContextRestoreGState(context);
}

-(void)drawFilledCircle:(CGFloat)center withRadius:(CGFloat)radius withFillColor:(UIColor*)fillColor
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorRef colorFromUiColor = [fillColor CGColor];
    
    CGContextSetFillColorWithColor(context, colorFromUiColor);
    
    //Draw ellipse as a circle (Equal height and width)
    CGRect ellipseRect = CGRectMake([self getCirclesOrigin:radius], [self getCirclesOrigin:radius], (radius * 2), (radius * 2));
    CGContextFillEllipseInRect(context, ellipseRect);
}

-(CGFloat)getCirclesOrigin:(CGFloat)circleRadius
{
    return [self getCenterPoint].x - circleRadius;
}

-(CGPoint)getCenterPoint
{
    CGFloat heightMiddle = self.bounds.size.height / 2.0f;
    CGFloat widthMiddle = self.bounds.size.width / 2.0f;
    
    return CGPointMake(widthMiddle, heightMiddle);
}
@end
