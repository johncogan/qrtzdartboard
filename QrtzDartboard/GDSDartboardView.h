//
//  GDSDartboardView.h
//  QrtzDartboard
//
//  Created by John Cogan on 2014/01/01.
//  Copyright (c) 2014 GeckoDigitalSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GDSDartboardView : UIView
{
    @private
    //CGPoint centerPoint;
    //CGContextRef _context;
    
}

//@property CGContextRef context;

-(CGPoint)getCenterPoint;

-(void)drawFilledCircle:(CGFloat)center withRadius:(CGFloat)radius withFillColor:(UIColor*)fillColor;

-(void)drawArc:(CGPoint)center withArcRadius:(CGFloat)arcRadius withStartAngle:(CGFloat)startAngle withThicknessAsPercentage:(CGFloat)innerBound withFillColor:(UIColor*)fillColor withContext:(CGContextRef)context;

-(CGFloat)rgbValueAsFractionOfOne:(CGFloat)rgbValue;

-(void)drawDoubles:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context;

-(void)drawTriples:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context;

-(void)drawSinglesOuter:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context;

-(void)drawSinglesInner:(CGFloat)startDegree withFirstColor:(UIColor*)colorOne withSecondColor:(UIColor*)colorTwo withContext:(CGContextRef)context;

- (void)drawTextInUILabel:(NSString*)text withOrigin:(CGPoint)origin withRotateAngle:(CGFloat)rotateAngle;

-(UILabel*)prepareUiLabel:(CGPoint)atOrigin withWidth:(CGFloat)frameWidth withHeight:(CGFloat)frameHeight;

-(CGPoint)getCartesianCoOrdinate:(CGFloat)radius withAngle:(CGFloat)angleFromZero;

-(void)drawNumbers;
@end

