//
//  GDSViewControllerTest.h
//  QrtzDartboard
//
//  Created by John Cogan on 2014/01/01.
//  Copyright (c) 2014 GeckoDigitalSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GDSDartboardView.h"

@interface GDSViewControllerTest : UIViewController

@property (weak, nonatomic) IBOutlet GDSDartboardView *viewDartBoard;

@end
